/**
 * Created by jesse on 1-3-16.
 */
window.addEventListener("load", function() {
    var popup = document.querySelector("#popup");
    var qrImg = document.querySelector("#qrimg");
    var addrs = document.querySelector("#url");
    document.querySelector("#gen").onmousedown = function() {
        if (addrs.value.length > 0) {
            qrImg.src = "http://nl.qr-code-generator.com/phpqrcode/getCode.php?cht=qr&chl="+encodeURIComponent(addrs.value)+"&chs=330x330&choe=UTF-8&chld=L|0";
            popup.style.display = "block";
        }
    };
    document.querySelector("#close").onmousedown = function() {
        popup.style.display = "none";
    };
});
