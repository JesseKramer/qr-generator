# QR-Code Generator

I did this project in about 20 minutes, I saw a website that could generate QR Codes with custom content.
Naturally when I did it, I looked for how it was done, and I found out the content was embedded in the URL of an HTTP request to a webservice.

I figured I could create a dedicated simple app that put the content in the URL, so here it is.

## DISCLAIMER

I did not write any of the generation logic. That was done by [qr-code-generator.com](qr-code-generator.com), and they host the webservice used in this project.
If either their website goes offline, or changes are made to the service, this app will malfunction. If this is the case, please report to [jesse.kramer@liquidpineapple.net](mailto:jesse.kramer@liquidpineapple.net).

Also, please excuse my ugly code, since this was written in 20 minutes and I couldn't find any more motivation afterwards to clean up the code. It is not a valid excuse, but I'm not asking for forgiveness, just for you to keep it in mind.